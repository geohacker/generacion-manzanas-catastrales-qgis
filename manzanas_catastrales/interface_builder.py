from qgis.core import *
from PyQt5.QtGui import *
import os

class InterfaceBuilder:
    def __init__(self, iface, dlg):
        self.iface = iface
        self.dlg = dlg
        self.raw_layers = self.get_layers()
        self.layers = {
            'sector': ["Seleccionar Sector"],
            'manzana': ["Seleccionar Manzana"],
            'loteo': ["Seleccionar Loteo"],
            'construccion': ["Seleccionar Construcción"],
            'placa': ["Seleccionar Placa Domi"],
            'malla_vial': ["Seleccionar Malla Vial"],
        }
        self.manzana_codes = []
        self.build_interface()

    def get_layers(self):
        return self.iface.mapCanvas().layers()

    def build_layers(self):
        # Identifico las capas de acuerdo con sus atributos
        for layer in self.raw_layers:
            if layer.geometryType() == 2:
                for field in layer.fields():
                    if field.name().upper() == 'SCACODIGO':
                        self.layers['sector'].append(layer.name())
                    elif field.name().upper() == 'MANCODIGO':
                        self.layers['manzana'].append(layer.name())
                    elif field.name().upper() == 'LOTCODIGO':
                        self.layers['loteo'].append(layer.name())
                    elif field.name().upper() in ["CODCODIGO", "CONNPISOS"]:
                        self.layers['construccion'].append(layer.name())
            elif layer.geometryType() == 0:
                for field in layer.fields():
                    if field.name().upper() in ["PDOCLOTE", "PDOANGULO", "PDOTEXTO"]:
                        if layer.name() not in self.layers["placa"]:
                            self.layers["placa"].append(layer.name())
            elif layer.geometryType() == 1:
                for field in layer.fields():
                    if field.name().upper() == 'MVIETIQUET':
                        self.layers["malla_vial"].append(layer.name())

        return self

    def set_combobox_layers(self):
        # Limpio primero los combobox para evitar selects repetidos
        self.dlg.sectorComboBox.clear()
        # Asigno la lista de opciones al combobox
        self.dlg.sectorComboBox.addItems(self.layers['sector'])

        self.dlg.ManzanaComboBox.clear()
        self.dlg.ManzanaComboBox.addItems(self.layers['manzana'])

        self.dlg.LoteoComboBox.clear()
        self.dlg.LoteoComboBox.addItems(self.layers['loteo'])

        self.dlg.ConstruccionComboBox.clear()
        self.dlg.ConstruccionComboBox.addItems(self.layers['construccion'])

        self.dlg.PlacaDomiComboBox.clear()
        self.dlg.PlacaDomiComboBox.addItems(self.layers['placa'])

        self.dlg.MallaVialComboBox.clear()
        self.dlg.MallaVialComboBox.addItems(self.layers['malla_vial'])

        return self

    def get_sector_attribute_value(self):
        current_sector_on_combobox = self.dlg.sectorComboBox.currentText()
        self.dlg.ManzanaComboBox.setEnabled(True)
        # Valido que el dato seleccionado no sea el por defecto
        if current_sector_on_combobox == self.layers['sector'][0]:
            self.dlg.ManzanaComboBox.setEnabled(False)
            self.dlg.sectorSCaCodigoComboBox.setEnabled(False)
            self.dlg.sectorSCaCodigoComboBox.clear()
            self.dlg.addButton.setEnabled(False)
        else:
            self.dlg.ManzanaComboBox.setEnabled(True)
            layer = QgsProject.instance().mapLayersByName(current_sector_on_combobox)[0]
            sectores = ["-----"]
            for feature in layer.getFeatures():
                sectores.append(feature.attributes()[0])
            sectores.sort()  # Ordeno el array
            self.dlg.sectorSCaCodigoComboBox.setEnabled(True)
            self.dlg.sectorSCaCodigoComboBox.clear()
            self.dlg.sectorSCaCodigoComboBox.addItems(sectores)

    def get_manzana_attribute_value(self):
        current_manzana_on_combobox = self.dlg.ManzanaComboBox.currentText()
        # Valido que el dato seleccionado no sea el por defecto
        if current_manzana_on_combobox == self.layers['manzana'][0]:
            self.dlg.manzanaManCodigoComboBox.setEnabled(False)
            self.dlg.manzanaManCodigoComboBox.clear()
            self.dlg.addButton.setEnabled(False)
        else:
            layer = QgsProject.instance().mapLayersByName(current_manzana_on_combobox)[0]
            current_sector_code_on_combobox = self.dlg.sectorSCaCodigoComboBox.currentText()
            self.dlg.manzanaManCodigoComboBox.setEnabled(True)
            manzanas = []
            for feature in layer.getFeatures():
                if feature.attributes()[1] == current_sector_code_on_combobox:
                    manzana_code = feature.attributes()[0]
                    self.manzana_codes.append(manzana_code)
                    manzanas.append(manzana_code[-3:])
            manzanas.sort()  # Ordeno el array
            self.dlg.manzanaManCodigoComboBox.clear()
            self.dlg.manzanaManCodigoComboBox.addItems(manzanas)
            if self.dlg.manzanaManCodigoComboBox.currentText() == '':
                self.dlg.addButton.setEnabled(False)
            else:
                self.dlg.addButton.setEnabled(True)

    def add_manzana_selected(self):
        manzana_to_add = self.dlg.manzanaManCodigoComboBox.currentText()
        manzana_code = self.dlg.sectorSCaCodigoComboBox.currentText() + manzana_to_add
        list_manzanas = []
        for index in range(self.dlg.listManzanas.count()):
            list_manzanas.append(self.dlg.listManzanas.item(index).text())
        if manzana_code in list_manzanas:
            pass
        else:
            list_manzanas.append(manzana_code)
            list_manzanas.sort()

        self.dlg.listManzanas.clear()
        self.dlg.listManzanas.addItems(list_manzanas)

    def validate_item_delete(self):
        if self.dlg.listManzanas.count() == 0:
            self.dlg.removeButton.setEnabled(False)
        else:
            if len(self.dlg.listManzanas.selectedItems()) > 0:
                self.dlg.removeButton.setEnabled(True)
            else:
                self.dlg.removeButton.setEnabled(False)

    def delete_manzana_selected(self):
        current_manzana_selected = self.dlg.listManzanas.selectedItems()
        for manzana in current_manzana_selected:
            self.dlg.listManzanas.takeItem(self.dlg.listManzanas.row(manzana))
        if self.dlg.listManzanas.count() == 0:
            self.dlg.removeButton.setEnabled(False)

    def generate_pdf(self):
        list_manzanas = []
        for index in range(self.dlg.listManzanas.count()):
            list_manzanas.append(self.dlg.listManzanas.item(index).text())

        #Traer las capas seleccionadas en el Plugin
        construccion = QgsProject.instance().mapLayersByName(self.dlg.ConstruccionComboBox.currentText())[0]
        sector = QgsProject.instance().mapLayersByName(self.dlg.sectorComboBox.currentText())[0]
        manzana = QgsProject.instance().mapLayersByName(self.dlg.ManzanaComboBox.currentText())[0]
        loteo = QgsProject.instance().mapLayersByName(self.dlg.LoteoComboBox.currentText())[0]
        placa = QgsProject.instance().mapLayersByName(self.dlg.PlacaDomiComboBox.currentText())[0]
        mallavial = QgsProject.instance().mapLayersByName(self.dlg.MallaVialComboBox.currentText())[0]

        self.simbology(construccion, 1)
        self.simbology(sector, 2)
        self.simbology(manzana, 3)
        self.simbology(loteo, 4)
        self.simbology(placa, 5)
        self.simbology(mallavial, 6)

        for codigo_manzana in list_manzanas:
            # Recorrer el listado de manzanas
            manzana.setSubsetString("\"ManCodigo\" = '" + codigo_manzana + "'")
            manzana.triggerRepaint()
            self.iface.mapCanvas().refresh()
            manzana.selectAll()
            self.iface.mapCanvas().zoomToSelected(manzana)
            manzana.removeSelection()
            self.iface.mapCanvas().refresh()
            self.export_pdf(codigo_manzana)
            #break

    def simbology(self, layer, type_layer):

        #Definir Labels
        text_format = QgsTextFormat()
        text_format.setFont(QFont ("Arial", 12))
        text_format.setSize(12)

        buffer_settings = QgsTextBufferSettings()
        buffer_settings.setEnabled(True)
        buffer_settings.setSize(0.1)
        buffer_settings.setColor(QColor("black"))

        text_format.setBuffer(buffer_settings)

        layer_settings = QgsPalLayerSettings()
        layer_settings.setFormat(text_format)

        #Defnir Simbologia
        simbolo= layer.renderer().symbol().symbolLayer(0)

        simbolo.setColor(QColor("black"))
        props= layer.renderer().symbol().symbolLayer(0).properties()
        #El type_layer permite identificar que capa es y poder establecer la simbologia

        if type_layer == 1:
            #Simbologia Construccion
            props['color_border'] = 'black'
            props['width_border'] = '0.8'
            props["style"] = "f_diagonal"
            layer_settings.fieldName = "ConNPisos"
        elif type_layer == 2:
            #Simbologia Sector
            props['color_border'] = 'gray'
            props['width_border'] = '1.4'
            props["style"] = "no"
            layer_settings.fieldName = "SCaNombre"
        elif type_layer == 3:
            #Simbologia Manzana
            props['color_border'] = 'black'
            props['width_border'] = '1'
            props["style"] = "no"
            layer_settings.fieldName = "ManCodigo"
        elif type_layer == 4:
            #Simbologia Loteo
            props['color_border']='black'
            props['width_border']='0.5'
            props["style"]="no"
            layer_settings.fieldName = "LotCodigo"
            text_format.setSize(5)
        elif type_layer == 5:
            #Simbologia Placa
            props['width_border']='0'
            props["style"]="no"
        elif type_layer == 6:
            #Simbologia Via
            props['width_border']='0'
            props["style"]="no"
            layer_settings.fieldName = "MVIEtiquet"

        #ShowLabel

        if type_layer in [1,2,3,4,6]:
            layer_settings.placement = 4
            layer_settings.enabled = True
            layer_settings = QgsVectorLayerSimpleLabeling(layer_settings)
            layer.setLabelsEnabled(True)
            layer.setLabeling(layer_settings)

        layer.renderer().setSymbol(QgsFillSymbol.createSimple(props))
        #show the changes
        layer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(layer.id()) 

    def export_pdf(self, codigo_manzana):
        print('Start pdf generate')
        project = QgsProject.instance()
        #get a reference to the layout manager 
        manager = project.layoutManager()
        #make a new print layout object
        layout = QgsPrintLayout(project)
        layoutName = "PrintLayout"
        layouts_list = manager.printLayouts()

        for layout in layouts_list:
            if layout.name() == layoutName:
                manager.removeLayout(layout)

        layout = QgsPrintLayout(project)
        #needs to call this according to API documentaiton 
        layout.initializeDefaults()
        # layout.setName(layoutName)
        layout_page = QgsLayoutItemPage(layout)
        layout_page.setPageSize(QgsLayoutSize(210, 297, QgsUnitTypes.LayoutMillimeters))
        layout_page.decodePageOrientation('Portrait')
        #add layout to manager
        ''' 
        manager.addLayout(layout)

        #ADD MAP
        #create default map canvas
        canvas = self.iface.mapCanvas()
        #rect = QgsRectangle(canvas.fullExtent())
        #rect.scale(1.2)
        #canvas.setExtent(rect)
        layout_map = QgsLayoutItemMap.create(layout)
        layout_map.setId("Mapa01")
        #using ndawson's answer below, do this before setting extent 
        #layout_map.attemptResize(QgsLayoutSize(4,4, QgsUnitTypes.LayoutInches))
        #layout_map.attemptMove(QgsLayoutPoint(3.5, 1.25, QgsUnitTypes.LayoutInches))
        #set an extent
        #layout_map.setExtent(rect) 
        #add the map to the layout
        layout.addLayoutItem(layout_map)
        '''
        '''

        #Add a legend to the print layout 
        legend = QgsLayoutItemLegend(layout) 
        legend.setTitle("Leyenda") 
        legend.setAutoUpdateModel(False) 
        legItems=legend.model() 
        layout.addItem(legend)
        '''
        '''
        
        #Create an object of QgsComposerLabel class 
        label = QgsLayoutItemLabel(layout) 
        label.setId("Titulo")
        label.setItemRotation(0, True) 
        label.setFont(QFont('Tahoma', 11)) 
        label.setFontColor(QColor('white')) 
        label.setText("MANZANA CATASTRAL ")
        label.adjustSizeToText() 
    
        #label.setFrameEnabled(True) 
        label.setBackgroundEnabled(True) 
        label.setBackgroundColor(QColor('blue'))
        label.attemptMove(QgsLayoutPoint(5, 0.25, QgsUnitTypes.LayoutInches)) 
        layout.addItem(label)
        
        


        #Create an object of QgsLayoutItemScaleBar 
        scalebar = QgsLayoutItemScaleBar(layout) 
        scalebar.setStyle('Single Box') 
        #scalebar.setStyle('Line Ticks Up')
         
        scalebar.setUnits(QgsUnitTypes.DistanceKilometers) 
        scalebar.setNumberOfSegments(3) 
        scalebar.setNumberOfSegmentsLeft(1) 
        scalebar.setUnitsPerSegment(100) 
        scalebar.setUnitLabel('km') 
        scalebar.setFont(QFont('Arial', 14)) 
        scalebar.setLinkedMap(layout_map) 
        #scalebar.applyDefaultSize()
        scalebar.update()
        scalebar.attemptMove(QgsLayoutPoint(20, 145, QgsUnitTypes.LayoutMillimeters)) 
        layout.addItem(scalebar)
        '''

        #exportando el Layout #
        layout_exporter = QgsLayoutExporter(layout)
        user_path = os.environ["HOME"] + "/Documents"
        pdf_export_settings = QgsLayoutExporter.PdfExportSettings()
        pdf_export_settings.dpi = 200
        res = layout_exporter.exportToPdf(user_path + '/' + codigo_manzana + '.pdf', pdf_export_settings)
        if res != QgsLayoutExporter.Success:
            raise RuntimeError()

        print('Salió el pdf en ' + user_path)
    # Creo la interface
    def build_interface(self):
        self.build_layers().set_combobox_layers()
